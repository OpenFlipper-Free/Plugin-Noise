/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/


#include "NoisePlugin.hh"

#include <iostream>

#include <OpenFlipper/BasePlugin/PluginFunctions.hh>

//-----------------------------------------------------------------------------

NoisePlugin::
NoisePlugin() :
    tool_(nullptr),
    toolIcon_(nullptr)
{

}

NoisePlugin::
~NoisePlugin()
{
    delete toolIcon_;
}



void
NoisePlugin::
initializePlugin()
{
  if ( OpenFlipper::Options::gui() ) {
    tool_ = new noiseToolbarWidget();
    QSize size(300, 300);
    tool_->resize(size);

    connect(tool_->addNoise, SIGNAL(clicked()), this, SLOT(slotAddNoise()));

    toolIcon_ = new QIcon(OpenFlipper::Options::iconDirStr()+OpenFlipper::Options::dirSeparator()+"NoiseIcon.png");

    emit addToolbox( tr("Noise") , tool_, toolIcon_ );
  }
}


//-----------------------------------------------------------------------------

void
NoisePlugin::
slotAddNoise()
{

  const double maxNoise = tool_->maxDistance->value();

  for (auto* o_it : PluginFunctions::objects(PluginFunctions::TARGET_OBJECTS) )
  {
    slotAddNoise(o_it->id(), maxNoise);
  }

}

void
NoisePlugin::
slotAddNoise(int _objectId, double _maxNoise)
{

  if (TriMesh* mesh = PluginFunctions::triMesh(_objectId))
  {
    slotAddNoise( mesh, _maxNoise );
    emit updatedObject(_objectId, UPDATE_GEOMETRY);
    emit createBackup( _objectId, "Noise", UPDATE_GEOMETRY);
  }
  else if  (PolyMesh* mesh = PluginFunctions::polyMesh(_objectId))
  {
    slotAddNoise( mesh, _maxNoise );
    emit updatedObject(_objectId, UPDATE_GEOMETRY);
    emit createBackup( _objectId, "Noise", UPDATE_GEOMETRY);
  }
  else if (SplatCloud* cloud = PluginFunctions::splatCloud(_objectId))
  {
    slotAddNoise( cloud, _maxNoise );
    emit updatedObject(_objectId, UPDATE_GEOMETRY);
    emit createBackup( _objectId, "Noise", UPDATE_GEOMETRY);
  }
}



template< class MeshT >
void
NoisePlugin::
slotAddNoise( MeshT* _mesh, double _maxNoise )
{
  if ( _mesh == nullptr )
  {
    emit log(LOGERR, "Unable to get mesh for object" );
    return;
  }

  if (!_mesh->has_face_normals())
  {
    _mesh->request_face_normals();
    _mesh->update_face_normals();
  }

  if (!_mesh->has_vertex_normals())
  {
    _mesh->request_vertex_normals();
    _mesh->update_vertex_normals();
  }

  for ( auto vh : _mesh->vertices())
  {
    double randomNumber = ( double(rand()) / double(RAND_MAX) * 2.0 - 1.0) * _maxNoise;
    _mesh->point(vh) = _mesh->point(vh) + _mesh->normal(vh) * randomNumber;
  }

  _mesh->update_normals();

}


void
NoisePlugin::
slotAddNoise( SplatCloud* _splat_cloud, double _maxNoise )
{
  if ( ! _splat_cloud->hasPositions() )
  {
    emit log(LOGERR, "Error: Splat cloud without positions!" );
    return;
  }

  if ( ! _splat_cloud->hasNormals() )
  {
    emit log(LOGINFO, "No normals available, adding noise in all directions" );

    for ( unsigned int i = 0 ; i < _splat_cloud->numSplats() ; ++i)
    {
      ACG::Vec3f direction(10.0,10.0,10.0);

      while (direction.sqrnorm() > 1.0)
      {
        direction[0] = (((double)rand()) / ((double)RAND_MAX) - 0.5) * 2.0;
        direction[1] = (((double)rand()) / ((double)RAND_MAX) - 0.5) * 2.0;
        direction[2] = (((double)rand()) / ((double)RAND_MAX) - 0.5) * 2.0;
      }

      direction.normalize();

      double randomNumber = ( double(rand()) / double(RAND_MAX) * 2.0 - 1.0) * _maxNoise;

      _splat_cloud->positions(i) = _splat_cloud->positions(i) + direction * randomNumber ;
    }
  }
  else
  {
    for ( unsigned int i = 0 ; i < _splat_cloud->numSplats() ; ++i)
    {
      double randomNumber = ( double(rand()) / double(RAND_MAX) * 2.0 - 1.0) * _maxNoise;
      _splat_cloud->positions(i) = _splat_cloud->positions(i) + _splat_cloud->normals(i) * randomNumber ;
    }
  }
}


